---
layout: page
title: "Anmeldung" # addresse unter der
sitemap: false
permalink: "/anmeldung.html"
teaser: "Jeder Platz beim alwizuko ist ein Geschenk,"
---
welches wir mit Freude jeweils 10 - 20 Menschen ermöglichen. Hoffentlich auch Dir! Für jedes Alwizuko nehmen ein paar Menschen die Verantwortung auf sich, finden einen geeigneten Ort, kümmern sich um leckere vegane Lebensmittel und müssen sich in so fern auf eure Zusage verlassen können. Wir wünschen uns daher eine größtmögliche Verbindlichkeit seitens aller Teilnehmer*innen und im Idealfall keine spontanen Absagen oder sogar nicht abgesagtes Nicht-Kommen!


### Hinweise zur Anmeldung
* Wir wünschen uns für die Atmosphäre Deine Anwesenheit an allen Tagen.
* Die Infrastruktur der jeweiligen Orte bringt einige Gegebenheiten mit sich, die du erfährst, sobald du dich angemeldet hast und wir dich (in Berücksichtigung deiner Präferenz) zugeteilt haben
* Als ein Mitmachraum von living utopia laden wir Dich ein während der alwizukos, die begleitenden Motive - solidarisch, ökologisch, vegan, drogenfrei u nd geldfrei - als Erfahrungsraum zu verstehen und zu erleben.
* Eine Anmeldung ist bestenfalls nur für Einzelpersonen gedacht (abgesehen von Babies/Kindern). Dies haben wir so entschieden, da wir angesichts der wenigen Plätze so fair wie möglich verlosen möchten und auch, weil der erwünschte intensive, gruppendynamische Prozess durch mehrere gemeinsam angemeldete “Grüppchen” gehemmt werden könnte. Solltest es dir dennoch sehr wichtig sein, mit einer bestimmten Person an dem selben Alwizuko teilzunehmen, dann meldet euch am besten zeitgleich und mit den selben Prioritäten an und schreibt uns diesen besonderen Wunsch unter "Sonstiges" in das Anmeldeformular.
* Mit deiner Anmeldung ist dir dein Platz noch nicht zu 100% sicher, da wir noch nicht wissen wie viele Menschen an einem Alwizuko teilnehmen möchten und wie viele Alwizukos angeboten werden. Wir teilen dir dies aber so bald wie möglich mit und hoffen auf dein Verständnis und deine Geduld. Wenn du dir aber sicher sein willst, dass es klappt, dann gibt es einen ganz einfachen Weg: Veranstalte selber ein Alwizuko! 😀 Das ist nicht viel Aufwand, wir helfen dir bei der Suche nach einem geeigneten Ort und du wirst von uns während des gesamten Prozesses begleitet!

## los gehts!

<form action="https://formspree.io/kontakt@alwizuko.de" method="POST">
  <div class="row">
    <div class="small-4 columns">
      <label for="name" class="right inline">Name*</label>
    </div>
    <div class="small-8 columns">
      <input type="text" id="name" name="nam.e" required>
    </div>
  </div>

  <div class="row">
    <div class="small-4 columns">
      <label for="email" class="right inline">E-mail*</label>
    </div>
    <div class="small-8 columns">
      <input type="email" id="email" name="emai.l" required>
    </div>
  </div>

  <div class="row">
    <div class="small-4 columns">
      <label for="tel" class="right inline">Telefonnummer</label>
    </div>
    <div class="small-8 columns">
      <input type="text" id="tel" name="te.l">
    </div>
  </div>

  <div class="row">
    <div class="small-4 columns">
      <label for="state" class="right inline">
        Bundesländer*<br />
        <small>Wohin kannst Du dir vorstellen zu einem Alwizuko zu fahren (welche Bundesländer kommen für dich in Frage)?</small>
      </label>
    </div>
    <div class="small-8 columns">
      <a href="#" onClick="setAll(true, event)">alle</a>
      <a href="#" onClick="setAll(false, event)">keine</a>
      <label><input name="state" type="checkbox" value="baden"> Baden-Württemberg</label>
      <label><input name="state" type="checkbox" value="bayern"> Bayern</label>
      <label><input name="state" type="checkbox" value="berlin"> Berlin</label>
      <label><input name="state" type="checkbox" value="brandenburg"> Brandenburg</label>
      <label><input name="state" type="checkbox" value="bremen"> Bremen</label>
      <label><input name="state" type="checkbox" value="hamburg"> Hamburg</label>
      <label><input name="state" type="checkbox" value="hessen"> Hessen</label>
      <label><input name="state" type="checkbox" value="mvp"> Mecklenburg-Vorpommern</label>
      <label><input name="state" type="checkbox" value="niedersachsen"> Niedersachsen</label>
      <label><input name="state" type="checkbox" value="nrw"> Nordrhein-Westfalen</label>
      <label><input name="state" type="checkbox" value="rheinland"> Rheinland-Pfalz</label>
      <label><input name="state" type="checkbox" value="saarland"> Saarland</label>
      <label><input name="state" type="checkbox" value="sachsen"> Sachsen</label>
      <label><input name="state" type="checkbox" value="sachsen-anhalt"> Sachsen-Anhalt</label>
      <label><input name="state" type="checkbox" value="schleswig"> Schleswig-Holstein</label>
      <label><input name="state" type="checkbox" value="thueringen"> Thüringen</label>

    </div>
  </div>

  <div class="row">
    <div class="large-12 columns">
      <label>Sonstiges
        <textarea name="sonstiges" placeholder="Gibt es noch etwas, was Du uns sagen möchtest? Was brauchst du um dich wohl zu fühlen? Hast du irgendwelche Allergien? Was wünscht du dir auf dem Alwizuko?" rows="5"></textarea>
      </label>
    </div>
  </div>

  <input type="hidden" name="_subject" value="[formspree] Anmeldung" />
  <input type="hidden" name="_language" value="de" />
  <input type="hidden" name="_next" value="https://alwizuko.de/anmeldung_erfolgreich.html" />
  <input type="hidden" name="_format" value="plain" />
  <input type="text" name="_gotcha" style="display:none" />

  <div class="row">
    <div class="small-6 columns ">
      <i>*Pflichtfelder</i>
    </div>
    <div class="small-6 columns text-right">
      <button>Anmelden</button>
    </div>
  </div>
</form>

<script>
function setAll(value,e) {
  e.preventDefault();
  e.stopPropagation();
  checkboxes = document.getElementsByName('state');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = value ? 'checked' : '';
  }
  return false;
}
</script>
